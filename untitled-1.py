#!/usr/bin/env python

import gpg, sys, os

def import_key_and_check_status(key):
    """Import a GnuPG key and check that the operation was successful.
    
    :param str key: A string specifying the key's filepath from
        ``Common.paths``, as well as its fingerprint in
        ``Common.fingerprints``.
    :rtype: bool
    :returns: ``True`` if the key is now within the keyring (or was
        previously and hasn't changed). ``False`` otherwise.
    """
    with gpg.Context() as c:
        # change home directory of current gpg context to torbrowser's gpg home
        gnupg_home = os.path.abspath('.local/share/torbrowser/gnupg_homedir')
        c.set_engine_info(gpg.constants.protocol.OpenPGP, '/usr/bin/gpg', gnupg_home)
        
        # try to gpg import key data
        c.op_import(gpg.Data(file=key))
        
        # store import results, if any
        result = c.op_import_result()
        if result:
            assert not result.considered == 0
            assert result.no_user_id == 0
            assert result.not_imported == 0
            return True
        else:
            return False

if __name__ == "__main__":
    tor_browser_developers = os.path.abspath('/usr/share/torbrowser-launcher/tor-browser-developers.asc')
    import_key_and_check_status(tor_browser_developers)
    
